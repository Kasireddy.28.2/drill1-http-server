const fs=require("fs").promises
const http=require("http")
const uuid=require("uuid")


let http_server=http.createServer(async(request,response)=>{
    try{
    if(request.url==="/html" && request.method==="GET"){
        let htmlData= await fs.readFile("./index.html","utf-8")
        response.writeHead(200,{"content-type":"html"})
        response.write(htmlData)
        response.end()

    }else if(request.url=="/json" && request.method=="GET"){
        let jsonData= await fs.readFile("./data.json","utf-8")
        response.writeHead(200,{"content-text":"json"})
        response.write(jsonData)
        response.end()
    }else if(request.url=="/uuid" && request.method=="GET"){
        response.writeHead(200,{"content":"uuid"})
        response.write(JSON.stringify({"uuid":uuid.v4()}))
        response.end()
    }else if(request.url.includes("/status") && request.method=="GET"){
        let statusCode=request.url.split("/")[2]
        response.writeHead(200,{"content-text":"status-code"})
        response.write(`Return a response with ${statusCode} status code`)
        response.end()
    }else if(request.url.includes("/delay") && request.method=="GET"){
        let delaySeconds=Number(request.url.split("/")[2])
        response.writeHead(200,{"content":"delay-seconds"})
        setTimeout(()=>{
            response.write(String(`The server should wait for ${delaySeconds} seconds and only then send a response with 200 status code.`))
            response.end()
        },delaySeconds)
    }else{
        response.writeHead(200,{"content-type":"plain-text"})
        response.end("INVALID URL")
    }
}catch(error){
    console.log(error)
}
})

http_server.listen(7000,()=>{
    console.log("server running in http://localhost:7000")
})
